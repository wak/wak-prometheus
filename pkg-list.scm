;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

(package (wak-prometheus (2) (1))
  (depends (srfi-1)
           (srfi-8)
           (srfi-9)
           (wak-common))
  
  (synopsis "prototype-based message-passing object system")
  (description
   "Prometheus is a prototype-based message-passing object system"
   "for Scheme similar to the Self language.")
  (homepage "http://www.forcix.cx/software/prometheus.html")
  
  (libraries (("prometheus" "private") -> ("wak" "prometheus" "private"))
             (sls -> "wak")))

;; Local Variables:
;; scheme-indent-styles: ((package 1))
;; End:
