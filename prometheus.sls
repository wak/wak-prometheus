;;; prometheus.sls --- R6RS library for the prometheus object system

;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:
#!r6rs

(library (wak prometheus)
  (export make-prometheus-root-object
          *the-root-object*
          define-method
          define-object)
  (import (except (rnrs base) error)
          (rnrs control)
          (only (srfi :1 lists) alist-cons)
          (srfi :8 receive)
          (srfi :9 records)
          (wak private define-values)
          (wak private include)
          (wak hermes))

  (define (error message . irritants)
    (assertion-violation "(wak prometheus)" message irritants))

  (include-file ((wak prometheus private) prometheus)))
